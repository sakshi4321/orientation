# Git Summary

Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency.It is a place where we can share files and contribute to various groups.

## Important terms
### Repository
A repository is a collection of source code.
### Remote Repository
 
It is a server where all the collaborators upload changes made to files. 
### Commits
A commit logs a change or series of changes that you have made to a file in the repository.
### Branches
A branch is essentially a unique set of code changes with a unique name. Each repository can have one or more branches. The main branch — the branch where all the changes eventually get merged into - is called the master.
### Staging
Before we make a commit, we must tell Git what files we want to commit. This is called staging and uses the add command. 
<img src="a.png" alt="drawing" width="400"/>

## Commands
### Commit Command
Commits all files from the staging area to local Repository
### Push Command	
Pushes all the changes made in local to Remote Repository
###Fetch Command
Collects the changes from Remote Repository and copies them to Local Repository but doesn't affect our workspace
### Pull Command
Collects the changes from Remote Repository and copies them to Local Repository along with merges to the current directory or our workspace
<img src="lifecycle.png" alt="drawing" width="400"/>

