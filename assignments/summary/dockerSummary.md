# Docker Summary
Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly.

<img src="d.jpg" alt="drawing" width="400"/>

## Docker Architecture


### Software
Also called dockerd, is a persistent process that manages Docker containers and handles container objects. 
### Objects

- A Docker container is a standardized, encapsulated environment that runs applications.The dockerd listens for requests sent via the Docker Engine API
- A Docker image is a read-only template used to build containers. Images are used to store and ship applications.
- A Docker service allows containers to be scaled across multiple Docker daemons. 
- A Docker container is a runnable instance of an image. You can create, start, stop, move, or delete a container using the Docker API or CLI.
### Registries
A Docker registry is a repository for Docker images.

<img src="n.jpg" alt="drawing" width="400"/>

## Commands
<img src="comm.png" alt="drawing" width="400"/>




